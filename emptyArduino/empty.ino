  


#define WIFIPOWER_PIN 4

#include <avr/wdt.h>

void setup() { 
  wdt_disable();
    
  // Setup locally attached sensors

  // Start WIFI
  pinMode(3, OUTPUT);
  digitalWrite(3, 1);
  

  // Set power on WIFI
  pinMode(WIFIPOWER_PIN, OUTPUT); 
  digitalWrite(WIFIPOWER_PIN, 0);

  wdt_enable(WDTO_8S);
}


void loop() { 
  wdt_reset();
    
}




