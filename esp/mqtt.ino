#include "PubSubClient.h"
#include "MyMessage.h"
#include "config.h"

#define MY_GATEWAY_MQTT_CLIENT
#define MY_GATEWAY_ESP8266



#if defined MY_CONTROLLER_IP_ADDRESS
IPAddress _brokerIp(MY_CONTROLLER_IP_ADDRESS);
#endif

#define EthernetClient WiFiClient

static EthernetClient _MQTT_ethClient;
static PubSubClient _MQTT_client(_MQTT_ethClient);

static bool _MQTT_connecting = true;
static bool _MQTT_available = false;
static MyMessage _MQTT_msg;

char _convBuffer[MAX_PAYLOAD*2+1];

#define GATEWAY_ADDRESS_				((uint8_t)0)		//!< Node ID for GW sketch


 bool protocolMQTTParse(MyMessage &message, char* topic, uint8_t* payload, unsigned int length);
 char * protocolFormatMQTTTopic(const char* prefix, MyMessage &message);
 bool protocolParse(MyMessage &message, char *inputString);
 char * protocolFormat(MyMessage &message);


bool gatewayTransportSend(MyMessage &message)
{
 	if (!_MQTT_client.connected()) {
 		return false;
 	}
 	char *topic = protocolFormatMQTTTopic(MY_MQTT_PUBLISH_TOPIC_PREFIX, message);

 #if defined(MY_MQTT_CLIENT_PUBLISH_RETAIN)
 	bool retain = mGetCommand(message) == C_SET ||
 	              (mGetCommand(message) == C_INTERNAL && message.type == I_BATTERY_LEVEL);
 #else
 	bool retain = false;
 #endif /* End of MY_MQTT_CLIENT_PUBLISH_RETAIN */
 	return _MQTT_client.publish(topic, message.getString(_convBuffer), retain);
}

void incomingMQTT(char *topic, uint8_t *payload, unsigned int length)
{
	 _MQTT_available = protocolMQTTParse(_MQTT_msg, topic, payload, length);
}

bool reconnectMQTT(void)
{
	// Attempt to connect
	if (_MQTT_client.connect(MY_MQTT_CLIENT_ID
#if defined(MY_MQTT_USER) && defined(MY_MQTT_PASSWORD)
	                         , MY_MQTT_USER, MY_MQTT_PASSWORD
#endif
	                        )) {

	//Serial.println("Connecting mqtt");
		_MQTT_client.subscribe(MY_MQTT_SUBSCRIBE_TOPIC_PREFIX "/+/+/+/+/+");
 //Serial.println("Conected mqtt");
    _MQTT_client.publish("mdms/init", "MQQT connected", false);
	IPAddress ip = WiFi.localIP();
	_MQTT_client.publish("mdms/ip", string2char(String(ip)), false);
	long rssi = WiFi.RSSI();
	_MQTT_client.publish("mdms/signal", string2char(String(rssi)), false);
		return true;
	}
	return false;
}

char* string2char(String command){
    if(command.length()!=0){
        char *p = const_cast<char*>(command.c_str());
        return p;
    }
}


bool gatewayTransportConnect(void)
{
	if (WiFi.status() != WL_CONNECTED) {
	    return false;
	}
	return true;
}


bool gatewayTransportInit(void)
{
	_MQTT_connecting = true;


#if defined(MY_CONTROLLER_IP_ADDRESS)
	_MQTT_client.setServer(_brokerIp, MY_PORT);
#else
	_MQTT_client.setServer(MY_CONTROLLER_URL_ADDRESS, MY_PORT);
#endif /* End of MY_CONTROLLER_IP_ADDRESS */

	_MQTT_client.setCallback(incomingMQTT);
	gatewayTransportConnect();

	_MQTT_connecting = false;
	return true;
}

bool gatewayTransportAvailable(void)
{
	if (_MQTT_connecting) {
		return false;
	}
	//keep lease on dhcp address
	//Ethernet.maintain();
	if (!_MQTT_client.connected()) {
		//reinitialise client
		if (gatewayTransportConnect()) {
			reconnectMQTT();
		}
		return false;
	}
	_MQTT_client.loop();
	return _MQTT_available;
}

MyMessage & gatewayTransportReceive(void)
{
	// Return the last parsed message
	_MQTT_available = false;
	return _MQTT_msg;
}

#define MY_GATEWAY_MAX_RECEIVE_LENGTH (100u)
typedef struct {
	// Suppress the warning about unused members in this struct because it is used through a complex
	// set of preprocessor directives
	// cppcheck-suppress unusedStructMember
	char string[MY_GATEWAY_MAX_RECEIVE_LENGTH];
	// cppcheck-suppress unusedStructMember
	uint8_t idx;
} inputBuffer;

static inputBuffer inputString;

MyMessage _ethernetMsg;
void mqttLoop(){

  if (_MQTT_client.connected()){

        while (Serial.available() > 0){
            const char inChar = Serial.read();
            if (inputString.idx < MY_GATEWAY_MAX_RECEIVE_LENGTH - 1) {
              // if newline then command is complete
              if (inChar == '\n' || inChar == '\r') {
                // Add string terminator and prepare for the next message
                inputString.string[inputString.idx] = 0;
              //	GATEWAY_DEBUG(PSTR("GWT:RFC:MSG=%s\n"), inputString.string);
                inputString.idx = 0;
                if (protocolParse(_ethernetMsg, inputString.string)) {
				  wdt_reset();
                  gatewayTransportSend(_ethernetMsg);
                }
                //break;
              } else {
                // add it to the inputString:
                inputString.string[inputString.idx++] = inChar;
              }
            } else {
              // Incoming message too long. Throw away
            //	GATEWAY_DEBUG(PSTR("!GWT:RFC:MSG TOO LONG\n"));
              inputString.idx = 0;
              // Finished with this client's message. Next loop() we'll see if there's more to read.
              break;
            }
              }
              
          };

		    wdt_reset();
if (gatewayTransportAvailable()){
     MyMessage msg = gatewayTransportReceive();
     //Serial.println("Msg received");
	 char *_ethernetMsg = protocolFormat(msg);
     Serial.write(_ethernetMsg);
  }
}


