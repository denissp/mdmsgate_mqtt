#include <ESP8266WebServer.h>
//#include <EEPROM.h>
//#include "config.h"
//#include "MSTcpServer.h"
//#include "WebServer.h"
//#include "eepromutils.h"
#include "PubSubClient.h"
#include "MyMessage.h"


#define DEBUG

#define MY_WIFI_SSID "Parhorest"
#define MY_WIFI_PASSWORD "dunjasha"

bool APConnect;
int APLastState = WL_IDLE_STATUS;
int APCStatus;
unsigned long APCLastTime;

#define ConnectTryTimeOut 10000

void setup() {
  wdt_disable();
  
	delay(1000);
	Serial.begin(115200);
	Serial.println();

  // Load params
  // EEPROM.begin(512);  
  // EEPROMReadStr(cAPName, ssId, sizeof(ssId));
  // EEPROMReadStr(cAPPass, ssPass, sizeof(ssPass));
  // EEPROMReadStr(cConnectAPName, APCSSId, sizeof(APCSSId));
  // EEPROMReadStr(cConnectAPPass, APCSSPass, sizeof(APCSSPass));
  // EEPROM.end();

  // if (ssId[0] == 0x00) {
  //   strcpy(ssId, WIFI_SSID);
  //   Serial.println("Set default SS ID");
  // }
  // if (ssPass[0] == 0x00){
  //   strcpy(ssPass, WIFI_PASS);
  //   Serial.println("Set default SS Password");
  // }
  // if (APCSSId[0] == 0x00) {
  //   strcpy(APCSSId, CAP_NAME);
  //   Serial.println("Set default Connect to AP");
  // }
  // if (APCSSPass[0] == 0x00){
  //   strcpy(APCSSPass, CAP_PASS);
  //   Serial.println("Set default Connect to Password");
  // }
   
  // Serial.printf("AP ID: %s\n", ssId);
  // Serial.printf("AP password: %s\n", ssPass);
  // Serial.printf("Connect AP: %s\n", APCSSId);
  // Serial.printf("Connect AP password: %s\n", APCSSPass);

  // check for the presence of the shield:
  if (WiFi.status() == WL_NO_SHIELD) {
    doutln("WiFi shield not present");
    // don't continue:
    while (true);
  }


  // WIFI
	/* You can remove the password parameter if you want the AP to be open. */
	//WiFi.softAP(ssId, ssPass);
  
	//IPAddress myIP = WiFi.softAPIP();
	//Serial.print("AP IP address: ");
	//Serial.println(myIP);

  //Serial.print("MAC: ");
  //Serial.println(WiFi.softAPmacAddress());

  // WebServer
 // WebServerInit();  

  //=== Config connect AP


  APCStatus = WiFi.begin(MY_WIFI_SSID, MY_WIFI_PASSWORD);
  doutln("Connected");
  APCLastTime = millis();

  // MySesnor Gate
  // MSTcpServerInit();
  gatewayTransportInit();
  
  wdt_enable(WDTO_8S);
}

void mqttLoop();




void loop() {  
  wdt_reset();
   
  // Web Server
  //WebServerHandle();


  // Connect to AP
  unsigned long currentTime = millis();
  if (APConnect && (APCStatus == WL_IDLE_STATUS) && (currentTime - APCLastTime > ConnectTryTimeOut)) {
    doutln("Attempting to connect to SSID: ");
    
    // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
   APCStatus = WiFi.begin(MY_WIFI_SSID, MY_WIFI_PASSWORD);
     doutln("Connected");
    APCLastTime = currentTime;
  }

  if ((APLastState != WL_CONNECTED) && (WiFi.status() == WL_CONNECTED)){
    printWifiStatus();
    APLastState = WL_CONNECTED;
  }

  // MySesnor Gate
 // MSTcpServerHandle();
 if (APLastState == WL_CONNECTED){
  mqttLoop();
 }

  
}

void printWifiStatus() {
  // print the SSID of the network you're attached to:
  dout("SSID1: ");
  doutln(WiFi.SSID());

  // print your WiFi shield's IP address:
  IPAddress ip = WiFi.localIP();
  dout("IP Address: ");
  doutln(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  dout("signal strength (RSSI):");
  dout(rssi);
  doutln(" dBm");
}

void doutln(String  s){
  #ifdef DEBUG
  Serial.println(s);
  #endif
}

void dout(String s){
  #ifdef DEBUG
  Serial.print(s);
  #endif
}

void dout(long s){
  #ifdef DEBUG
  Serial.print(s);
  #endif
}
void doutln(IPAddress s){
  #ifdef DEBUG
  Serial.println(s);
  #endif
}
